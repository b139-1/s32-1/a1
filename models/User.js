const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
	{
		firstname: {
			type: String,
			required: [true, 'First name is required']
		},
		lastname: {
			type: String,
			required: [true, 'Last name is required']
		},
		email: {
			type: String,
			required: [true, 'Email address is required']
		},
		password: {
			type: String,
			required: [true, 'Password is required']
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		mobileNo: {
			type: String,
			required: [true, 'Please provide mobile number']
		},
		enrollments: [
			{
				courseId: {
					type: String,
					required: [true, 'Course ID is required']
				},
				enrolledOn: {
					type: Date,
					dafault: new Date()
				},
				status: {
					type: String,
					default: 'Enrolled'
				}
			}
		]
	}
);

module.exports = mongoose.model("User", userSchema);