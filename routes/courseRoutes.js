const express = require("express");
const router = express.Router();
const courseController = require("./../controllers/courseControllers.js");

router.post("/create-course", (req, res) => {
	courseController.createCourse(req.body).then(result => res.send(result));
})

router.get("/", (req, res) => {
	courseController.getAllCourses().then(result => res.send(result))
})

router.get("/active-courses", (req, res) => {
	courseController.getActiveCourses().then(result => res.send(result))
})

//specific course findOne()
router.get("/find-course", (req, res) => {
	courseController.findCourse(req.body).then(result => res.send(result))
})

//specific course findById()
router.get("/find-course/:id", (req, res) => {
	courseController.findCourseId(req.params.id).then(result => res.send(result))
})

//update isActive course findOneAndUpdate()
router.put("/update-courses", (req, res) => {
	courseController.updateCourseStatus(req.body).then(result => res.send(result))
})

//update isActive course findByIdAndUpdate()
router.put("/update-courses/:id", (req, res) => {
	courseController.updateCourseStatusId(req).then(result => res.send(result))
})

//delete course findOneAndDelete()
router.delete("/delete-course", (req, res) => {
	courseController.deleteCourse(req.body).then(result => res.send(`Course ${req.body.courseName} has been deleted!`))
})

//delete course findByIdAndDelete()
router.delete("/delete-course/:id", (req, res) => {
	courseController.deleteCourseId(req.params.id).then(result => res.send(`Course deleted!`))
})


module.exports = router;