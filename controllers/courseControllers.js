const Course = require("./../models/Course.js");

module.exports.createCourse = (data) => {
	let newCourse = new Course({
		courseName: data.courseName,
		description: data.description,
		price: data.price
	});

	return newCourse.save().then((result, err) => {
		if(err) {
			return false;
		} else {
			return true;
		}
	});
}

module.exports.getAllCourses = () => {
	return Course.find().then((result, err) => {
		if(err) {
			return false;
		} else {
			return result;
		}
	});
}

module.exports.getActiveCourses = () => {
	return Course.find({isActive: true}).then((result, err) => {
		if(err) {
			return false
		} else {
			if(result == null) {
				return `Course does not exist!`
			} else {
				return result
			}
		}
	})
}

module.exports.findCourse = (data) => {
	return Course.findOne({courseName: data.courseName}).then((result, err) => {
		if(err) {
			return false
		} else {
			if(result == null) {
				return `Course does not exist! Please try again...`
			} else {
				return result
			}
		}
	})
}

module.exports.findCourseId = (data) => {
	return Course.findById(data).then((result, err) => {
		if(err) {
			return false
		} else {
			if(result == null) {
				return `Course does not exist! Please try again...`
			} else {
				return result
			}
		}
	})
}

module.exports.updateCourseStatus = (data) => {
	return Course.findOneAndUpdate({courseName: data.courseName}, {isActive: data.isActive}).then((result, err) => {
		if(err) {
			return false
		} else {
			if(result == null) {
				return `Course does not exist! Please try again...`
			} else {
				return result
			}
		}
	})
}

module.exports.updateCourseStatusId = (data) => {
	return Course.findByIdAndUpdate(data.params.id, {isActive: data.body.isActive}).then((result, err) => {
		if(err) {
			return false
		} else {
			if(result == null) {
				return `Course does not exist! Please try again...`
			} else {
				return result
			}
		}
	})
}

module.exports.deleteCourse = (data) => {
	return Course.findOneAndDelete({courseName: data.courseName}).then((result, err) => {
		if(err) {
			return false
		} else {
			if(result == null) {
				return `Course does not exist! Please try again...`
			} else {
				return result
			}
		}
	})
}

module.exports.deleteCourseId = (data) => {
	return Course.findByIdAndDelete(data).then((result, err) => {
		if(err) {
			return false
		} else {
			if(result == null) {
				return `Course does not exist! Please try again...`
			} else {
				return result
			}
		}
	})
}