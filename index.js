const express = require("express");
const mongoose = require("mongoose");
const app = express();
const courseRoutes = require("./routes/CourseRoutes.js")
const PORT = process.env.PORT || 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/api/courses", courseRoutes)

mongoose.connect('mongodb+srv://kaisertabuada:2qjhsn9Q@batch139.f3dzn.mongodb.net/course-booking?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection error:'));
db.once('open', () => console.log("Connected to database..."));


app.listen(PORT, () => console.log(`Server running at port ${PORT}...`));